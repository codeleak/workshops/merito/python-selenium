import os

from selenium import webdriver
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.chrome.options import Options as ChromeOptions

from common import config  # import global properties


def new_driver(driver="chrome", implicitly_wait=1) -> WebDriver:
    if driver == "firefox":
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', config.TMP_DIR)
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv, text/txt, application/vnd.ms-excel')
        profile.set_preference('general.warnOnAboutConfig', False)
        driver = webdriver.Firefox(firefox_profile=profile)
    if driver == "chrome":
        # driver = webdriver.Chrome()
        options = ChromeOptions()
        # the path must be absolute, otherwise Chrome won't download the file
        options.add_experimental_option("prefs", {"download.default_directory": os.path.abspath(config.TMP_DIR)})
        driver = webdriver.Chrome(options=options)
    driver.implicitly_wait(implicitly_wait)
    # driver.maximize_window()
    return driver


def click(driver, css_selector) -> None:
    elements = driver.find_elements_by_css_selector(css_selector)
    if len(elements) > 0 and not elements[0].is_disabled():
        elements[0].click()


def is_displayed(driver, css_selector) -> bool:
    elements = driver.find_elements_by_css_selector(css_selector)
    return len(elements) > 0 and elements[0].is_displayed()


def is_disabled(driver, css_selector) -> bool:
    elements = driver.find_elements_by_css_selector(css_selector)
    return len(elements) > 0 and elements[0].is_disabled()


def wait_until_displayed(driver, css_selector) -> bool:
    from selenium.webdriver.support.wait import WebDriverWait
    return WebDriverWait(driver=driver, timeout=5) \
        .until(lambda driver: driver.find_element_by_css_selector(css_selector).is_displayed())


def wait_until_disappeared(driver, css_selector) -> bool:
    from selenium.webdriver.support.wait import WebDriverWait
    return WebDriverWait(driver=driver, timeout=5) \
        .until_not(lambda _: driver.find_element_by_css_selector(css_selector).is_displayed())
