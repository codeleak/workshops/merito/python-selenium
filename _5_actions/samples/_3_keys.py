from time import sleep

import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from common import helpers


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get("https://qalabs.pl/demos/web-samples/sample1.html")
    yield driver
    driver.close()


def test_close_dialog_with_escape_key(driver):
    driver.find_element(By.CSS_SELECTOR, "button").click()
    dialog = driver.find_element(By.CSS_SELECTOR, ".md-dialog")

    actions = ActionChains(driver)
    actions.send_keys_to_element(dialog, Keys.ESCAPE).perform()

    sleep(1)

    assert len(driver.find_elements(By.CSS_SELECTOR, ".md-dialog")) == 0
