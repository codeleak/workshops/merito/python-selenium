from time import sleep

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample6.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_wait_for_progress_bar_to_finish(driver):
    # TODO Find and click Load button
    # TODO Wait for progress bar to finish
    assert "width: 100%;" == driver.find_element(By.CSS_SELECTOR, ".md-progress-bar .md-progress-bar-fill").get_attribute("style")
