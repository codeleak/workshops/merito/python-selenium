import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver(implicitly_wait=0.25)
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample4.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_dont_wait_for_snackbar(driver):
    submit = driver.find_element(By.ID, "submit")
    submit.click()

    snackbar = driver.find_element(By.ID, "snackbar")

    snackbar_text = driver.find_element(By.CSS_SELECTOR, ".md-snackbar-content > span").text
    assert "Form submitted!" == snackbar_text


def test_wait_for_snackbar(driver):
    submit = driver.find_element(By.ID, "submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    wait.until(ec.visibility_of_element_located((By.ID, "snackbar")))

    snackbar_text = driver.find_element(By.CSS_SELECTOR, ".md-snackbar-content > span").text
    assert "Form submitted!" == snackbar_text


def test_wait_for_snackbar_text(driver):
    submit = driver.find_element(By.ID, "submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    snackbar_text_present = wait.until(
        ec.text_to_be_present_in_element((By.CSS_SELECTOR, ".md-snackbar-content > span"), "Form submitted!"))

    assert snackbar_text_present


def test_wait_for_snackbar_element(driver):
    submit = driver.find_element(By.ID, "submit")
    submit.click()
    wait = WebDriverWait(driver, 5)
    snackbar = wait.until(ec.visibility_of_element_located((By.ID, "snackbar")))

    assert snackbar.is_displayed()


def test_wait_for_snackbar_text_with_lambda(driver):
    submit = driver.find_element(By.ID, "submit")
    submit.click()

    wait = WebDriverWait(driver, 5)
    snackbar_text = wait.until(lambda _: driver.find_element(By.CSS_SELECTOR, ".md-snackbar-content > span").text)

    assert "Form submitted!" == snackbar_text
