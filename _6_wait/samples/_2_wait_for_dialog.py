import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get("https://qalabs.pl/demos/web-samples/sample6.html")
    yield driver
    driver.close()


def test_wait_for_dialog(driver):
    show_dialog_button = driver.find_element(By.XPATH, "//*[text() = 'Show dialog']")
    show_dialog_button.click()

    wait = WebDriverWait(driver, 30)
    dialog_shown = wait.until(
        ec.text_to_be_present_in_element((By.CSS_SELECTOR, ".md-dialog span.md-dialog-title"), "Dialog"))
    assert dialog_shown
    assert "Lorem ipsum dolor sit amet" in driver.find_element(By.CSS_SELECTOR, ".md-dialog .md-dialog-content").text
