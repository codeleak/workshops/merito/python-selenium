from time import sleep

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample1.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_wait_for_dialog_to_be_shown(driver):
    # TODO Set input value to "WebDriverWait"
    # TODO Click the button
    # TODO Wait for dialog to show up and to contain text "Hello, WebDriverWait"
    pytest.fail("Not implemented! Remove this line.")
