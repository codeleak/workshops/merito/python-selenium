Test Automation with Python, Pytest and Selenium
----


<!-- TOC -->
* [Prerequisites](#prerequisites)
* [Setup project with PyCharm](#setup-project-with-pycharm)
* [Manual setup with CLI](#manual-setup-with-cli)
  * [Setup on Windows](#setup-on-windows)
  * [Setup on macOS](#setup-on-macos)
  * [Verify project setup in terminal](#verify-project-setup-in-terminal)
* [Troubleshooting](#troubleshooting)
  * [How to activate the virtual environment after you restart shell?](#how-to-activate-the-virtual-environment-after-you-restart-shell)
  * [`PyCharm` not discovering `venv`](#pycharm-not-discovering-venv)
  * [Remove run configurations](#remove-run-configurations)
    * [Upgrading project dependencies](#upgrading-project-dependencies)
* [See also](#see-also)
* [Upgrade dependencies](#upgrade-dependencies)
  * [Python](#python)
  * [Upgrade pip and setuptools](#upgrade-pip-and-setuptools)
  * [Upgrade dependencies](#upgrade-dependencies-1)
<!-- TOC -->

# Prerequisites

- Python 3.11
- PyCharm 2022.3.2 (Community Edition)
- Up-to-date `Chrome` and `Firefox` browsers

> Note: update the browsers before running tests in this project.

- Optionally: Git, Terminal (e.g. [cmder](https://blog.qalabs.pl/narzedzia/git-cmder/)). Recommended: cmder full on Windows.

# Setup project with PyCharm

> Verified on macOS with PyCharm 2022.3.2 (Community Edition) 

- Open PyCharm

![pycharm-welcome.jpg](docs/pycharm-welcome.jpg)

- Select `Get from VCS`
- Paste the URL of this repository: `https://gitlab.com/codeleak/workshops/python-selenium`

![pycharm-get-from-vcs.jpg](docs/pycharm-get-from-vcs.jpg)

- Click `Clone`
- Wait for the project to be imported.
- Open `_0_env_test/env_test.py`
- Click `Configure Python interpreter` in the top of the editor
- Select `Add New Interpreter > Add Local Interpreter`

![pycharm-editor.jpg](docs/pycharm-editor.jpg)

- Virtual Environment setup:
  - Environment: `New`
  - Location: as suggested by PyCharm
  - Base interpreter: `Python 3.11` you have installed on your machine
  - Click `OK`

![pycharm-venv.jpg](docs/pycharm-venv.jpg)

- While still in editor, click `Install requirements.txt` in the top of the editor

![pycharm-reqs.jpg](docs/pycharm-reqs.jpg)

- Wait for the dependencies to be installed

- In the project explorer navigate to `_0_env_test`

![pycharm-project-view.jpg](docs/pycharm-project-view.jpg)

- Right click on `env_test.py` and select `Run 'Python tests in ...'`

![pycharm-run-tests.jpg](docs/pycharm-run-tests.jpg)

- Expected results:

![ycharm-tests-results.jpg](docs/pycharm-tests-results.jpg)

# Manual setup with CLI

> Note: This is entirely optional for the workshops.

## Setup on Windows

- Open terminal
- Navigate to project directory
- Create and activate virtual environment by running:

```
venv-install.bat
```

- Install dependencies:

```
pip install -r requirements.txt
```

- Verify project setup by running:

```
python -m pytest _0_env_test/env_test.py
```

- Import project to PyCharm

## Setup on macOS

- Open terminal
- Create and activate virtual environment

```
python -m venv venv
source venv/bin/activate
```

- Install dependencies:

```
pip install -r requirements.txt
```

- Verify project setup by running:

```
python -m pytest _0_env_test/env_test.py
```

- Import project to PyCharm


## Verify project setup in terminal


- Open terminal
- Navigate to project directory
- Activate virtual env for your project
- Execute the following command:

```
python -m pytest _0_env_test/env_test.py
```

- Expected results:
    - Firefox and Chrome tests executed successfully (you should actually see the browsers starting and closing)
    - Screenshots created in `screenshots` directory
    - In the terminal you should see that 3 out of 3 tests passed
  
```
(venv) ➜  python-selenium python -m pytest _0_env_test/env_test.py
================================================= test session starts ==================================================
platform darwin -- Python 3.8.6, pytest-6.2.2, py-1.10.0, pluggy-0.13.1
rootdir: /Users/kolorobot/python-selenium
collected 3 items

_0_env_test/env_test.py ...                                                                                      [100%]

=================================================== warnings summary ===================================================
_0_env_test/env_test.py::test_chrome
  /Users/kolorobot/python-selenium/_0_env_test/env_test.py:45: DeprecationWarning: use options instead of chrome_options
    driver = webdriver.Chrome(

-- Docs: https://docs.pytest.org/en/stable/warnings.html
============================================ 3 passed, 1 warning in 11.10s =============================================
(venv) ➜  python-selenium
```


# Troubleshooting

## How to activate the virtual environment after you restart shell?

- Open terminal
- Navigate to project directory and execute:

```
venv/Scripts/activate.bat
```

## `PyCharm` not discovering `venv`

The virtual environment previously setup should be automatically detected by `PyCharm`. If for some reason, it was not, you must configure the project SDK manually. Follow the official tutorial on configuring the virtual environment:

https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html

On other issue might be that you have used spaces in the path of your project. Make sure you don't use spaces in the project path.

## Remove run configurations

If you happen to run the test file without `pytest` set as default test runner, you may experience issues when re-running the test. You may need to clean up previously created run configurations.

- Open `Run > Edit Configurations...`
- Remove existing run configurations
- Apply changes

### Upgrading project dependencies

```
pip install pytest
pip install selenium
pip install webdriver-manager
```

# See also

- Check `common` package to see configuration options

# Upgrade dependencies

> Note: This is for the project maintainer only.

## Python

- Project tested with Python 3.10 and 3.11. To upgrade and test with newer Python version with `asdf`, run:

  
    asdf python install <VERSION>
    asdf global python <VERSION>
    python -V

> Note: After a macOS upgrade, you may need to reinstall Xcode Command Line Tools from the command line:

    xcode-select --install

- Recreate virtual environment and re-configure PyCharm


    rm -rf venv
    python -m venv venv
    source venv/bin/activate

## Upgrade pip and setuptools

    pip install --upgrade pip setuptools

## Upgrade dependencies

- Check which dependencies are outdated:


    pip list --outdated

- In `requirements.txt` file, change the version of the dependency to the latest one or remove the version constraint completely. For example, change:


    selenium==3.141.0 to selenium>=3.141.0

> Tip: Use find and replace for all versions.

- Upgrade dependencies:


    pip install -r requirements.txt --upgrade

- Freeze dependencies:


    pip freeze > requirements.txt
