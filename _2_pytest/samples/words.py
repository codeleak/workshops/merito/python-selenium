def reverse(word: str) -> str:
    return word[::-1]
