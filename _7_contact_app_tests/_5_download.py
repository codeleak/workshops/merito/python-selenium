import csv
import os

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from common import config


@pytest.fixture
def driver(login):
    _remove_downloaded_csv_files()
    # Returns already logged in user
    return login


def test_download_all_contacts(driver):
    # TODO Implement. Implement with WebDriverWait.
    pytest.fail("Not implemented! Remove this line.")


def test_download_selected_contacts(driver):
    # TODO Implement. Implement with WebDriverWait.
    pytest.fail("Not implemented! Remove this line.")


def _remove_downloaded_csv_files():
    files = [file for file in os.listdir(config.TMP_DIR) if file.endswith(".csv")]
    for file in files:
        os.remove(config.TMP_DIR + os.sep + file)


def _read_contacts_from_csv():
    with open(file=f"{config.TMP_DIR}{os.sep}contacts.csv", mode="r") as csv_file:
        csv_reader = csv.DictReader(csv_file)
        data = []
        for row in csv_reader:
            data.append(row)
    return data
