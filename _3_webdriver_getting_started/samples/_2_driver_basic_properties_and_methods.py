from common import config, helpers


def test():
    # Init driver
    driver = helpers.new_driver("chrome")

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get("https://qalabs.pl/demos/web-samples/sample1.html")

    # Maximize window
    driver.maximize_window()

    # Delete all cookies
    driver.delete_all_cookies()

    # Save screenshot
    driver.save_screenshot(config.SCREENSHOTS_DIR + "/" + driver.title.replace(" ", "_").lower() + ".png")

    # Verify some properties
    assert driver.name == "firefox" or driver.name == "chrome"
    assert driver.title == "Sample 1"
    assert "sample1.html" in driver.current_url

    # Close the driver
    driver.close()
