import os

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def test_firefox():
    firefox_options = webdriver.FirefoxOptions()
    firefox_options.set_preference('browser.download.folderList', 2)
    firefox_options.set_preference('browser.download.manager.showWhenStarting', False)
    firefox_options.set_preference('browser.download.dir', os.getcwd())
    firefox_options.set_preference('browser.helperApps.neverAsk.saveToDisk', '*/*')
    firefox_options.set_preference('general.warnOnAboutConfig', False)

    driver = webdriver.Firefox(options=firefox_options)

    # Wait until page is loaded
    driver.get("https://qalabs.pl/demos/web-samples/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()


def test_chrome():
    chrome_options = Options()
    chrome_options.accept_insecure_certs = True
    # chrome_options.add_argument(f"--user-data-dir={config.TMP_DIR}/chrome-data-dir")

    driver = webdriver.Chrome(options=chrome_options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get("https://qalabs.pl/demos/web-samples/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()
