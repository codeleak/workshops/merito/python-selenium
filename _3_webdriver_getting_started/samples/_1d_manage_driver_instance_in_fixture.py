import pytest

from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample1.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver):
    assert "Sample 1" == driver.title


def test_2(driver):
    assert "Sample 1" == driver.title


def test_3(driver):
    assert "Sample 1" == driver.title
