import pytest
from selenium.webdriver import Firefox

from common import config


@pytest.fixture
def driver():
    driver = Firefox()
    driver.get("https://qalabs.pl/demos/web-samples/sample1.html")
    yield driver
    driver.close()


def test_1(driver):
    assert "Sample 1" == driver.title


def test_2(driver):
    assert "Sample 1" == driver.title
