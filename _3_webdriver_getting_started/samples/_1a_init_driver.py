from selenium.webdriver import Firefox


def test():
    # Init firefox with Gecko driver and Firefox executables
    driver = Firefox()

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get("https://qalabs.pl/demos/web-samples//sample1.html")

    # Print page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()
