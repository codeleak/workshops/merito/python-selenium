import pytest
from selenium.webdriver import Chrome


# TODO Make sure there is a single browser instance opened (fixture scope="module")
@pytest.fixture
def driver():
    driver = Chrome()
    driver.implicitly_wait(2)
    yield driver
    driver.close()


# TODO Create tests that loads sample1.html through sample3.html (get).
# TODO In each test add basic assertions.
# TODO Compare execution time with and without scope="module" in fixture

def test_get_sample_1(driver):
    pytest.fail("Not implemented! Remove this line.")


def test_get_sample_2(driver):
    pytest.fail("Not implemented! Remove this line.")


def test_get_sample_3(driver):
    pytest.fail("Not implemented! Remove this line.")
