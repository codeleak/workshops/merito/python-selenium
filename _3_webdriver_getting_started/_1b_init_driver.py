import pytest
from selenium.webdriver import Chrome


# TODO Add @pytest.fixture decorator
def driver():
    driver = Chrome()
    driver.implicitly_wait(2)
    yield driver
    driver.close()


# TODO Add `driver` parameter
def test_get_sample1():
    # TODO Get https://qalabs.pl/demos/web-samples/sample1.html
    # TODO Assert page title is "Sample 1"
    # TODO Assert page url is "https://qalabs.pl/demos/web-samples/sample1.html"
    pytest.fail("Not implemented! Remove this line.")


# TODO Add `driver` parameter
def test_get_sample2():
    # TODO Get https://qalabs.pl/demos/web-samples/sample2.html
    # TODO Assert page title is "Sample 2"
    # TODO Assert page url is "https://qalabs.pl/demos/web-samples/sample2.html"
    pytest.fail("Not implemented! Remove this line.")
